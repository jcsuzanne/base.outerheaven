(pour lire le fichier readme correctement, installer l'extension Chrome [Markdown Preview](https://chrome.google.com/webstore/detail/markdown-preview/jmchmkecamhbiokiopfpnfgbidieafmd))

# comment utiliser OuterHeaven
Voici le processus d'installation d'un nouveau projet

## Installation du projet

1. R&eacute;cup&eacute;rer le script install.sh dans le [depot git](https://bitbucket.org/jcsuzanne/base.outerheaven/raw/b32e0d96652200ec8a53861208b80611e2dca3f1/install.sh)
2. Le copier dans le dossier du projet et lancer le script via la commande

### chemin/du/projet

    ./install.sh

Si ca ne marche pas, mettre à jour les droits du fichier

### chemin/du/fichier

    chmod +x /path/to/yourscript.sh

## Pr&eacute;parer le nouveau projet

Quelques actions &agrave; r&eacute;aliser au lancement du projet

1. Changer le Rewrite Base dans le fichier **.htaccess** si necessaire
2. Changer la constante PHP/JS **WEB_ROOT** dans la configuration locale du projet [conf.local.inc.php](conf.local.inc.php)
3. Choisir un namespace JS et le modifier dans les fichiers JS [com.core.js](assets/js/com.core.js) et [com.toolbox.js](assets/js/com.toolbox.js)
4. Tester le bon fonctionnement du projet en allant sur la homepage du projet [Accueil](./)
5. R&eacute;cup&eacute;ration de Grunt et de ses d&eacute;pendances
6. V&eacute;rifier le bon fonctionnement de Grunt en executant la commande *grunt default* dans votre Terminal
7. Choisir le mode de developpement du projet : Site statique ou dynamique? Fait-on appel aux sources CSS/JS minifi&eacute;es? Dois-je me connecter au BO en distant?
8. Passer sur la branche git **DEVELOP**

## R&eacute;cup&eacute;ration de Grunt et des plugins Grunt

Les diff&eacute;rentes manipulations de fichiers (compilation LESS>CSS, concat&eacute;nation de fichiers, minification, etc...) sont r&eacute;alis&eacute;es avec [Grunt](http://gruntjs.com/getting-started)

Pour r&eacute;cup&eacute;rer Grunt et ses d&eacute;pendances, une seule ligne de commande a &eacute;xecut&eacute; dans votre *Terminal* dans &agrave; la racine du nouveau projet

### chemin/du/projet

    npm install

Si l'installation &eacute;choue, v&eacute;rifier que Grunt est correctement install&eacute; dans votre environnement

## Mode de d&eacute;veloppement du projet

Tous les modifications ci-dessous sont &agrave; r&eacute;aliser dans le fichier [conf.local.inc.php](conf.local.inc.php)

### Statique ou dynamique

    $conf_phpJs['MODE_TEMPLATE']    = true;

### Ne pas lire les sources minifi&eacute;es

    $conf_phpJs['MODE_COMPRESSION'] = false;

Par d&eacute;faut, le site est connect&eacute; au BO d'*Isabel Marant* donc pas d'inqui&eacute;tude si vous retrouvez les informations de ce projet lors du switch en mode TEMPLATE.

## Comment ajouter des nouveaux fichiers LESS, CSS ou JS?

C'est tr&egrave;s simple, il faut juste modifier la configuration du fichier [Gruntfile.js](Gruntfile.js) au niveau :

* du module **concat** pour les fichiers CSS et JS
* du module **recess** pour les fichiers LESS

et de reg&eacute;nerer les fichiers via la ligne de commande

### chemin/du/projet

    grunt default

## Mode statique

Avec au pr&eacute;alable :

    $conf_phpJs['MODE_TEMPLATE']    = false;

Pour acceder aux fichiers statiques, il suffit tout simplement de les cr&eacute;er dans le dossier **static** en respectant la nomenclature suivante *votre_fichier.inc.php*
Ensuite dans le navigateur, allez sur *votre/domaine/fr/votre_fichier

## Mode dynamique

* Pour avoir diff&eacute;rentes informations sur le template associ&eacute; &agrave; l'url, rajouter le param&egrave;tre GET **?console** dans l'url
* Pour avoir acc&egrave;s aux datas disponibles &agrave; traiter sur le template, rajouter le param&egrave;tre GET **?showJSON** dans l'url

# PHP Tips

# Biblioth&egrave;ques de ressources

Les diff&eacute;rentes ressources FO que j'utilise sont repertori&eacute;s &agrave; cette adresse [ressources](https://bitbucket.org/jcsuzanne/js-ressources/src).

Pour &eacute;viter d'avoir &agrave; les chercher dans les diff&eacute;rents projets, etc...
Donc libre &agrave; vous de venir piocher, d'y participer (dans ce cas la, demandez-moi un acc&egrave;s) ou pas!

# Git cmd

Pour retirer un fichier de l'index des fichiers modifi&eacute;s

    git update-index --assume-unchanged <file>

# Autres
* [UN Coding Styleguide](http://dev.ultranoir.com/ressources/)
* [Kickstarter.js](http://dev.ultranoir.com/ressources/kickstarter.js/)


### Calling json views

    <script type="text/javascript" src="<?php echo WEB_ROOT.'views.php' ?>"></script>

