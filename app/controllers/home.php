<?php
// Modele
//=======
if(MODE_TEMPLATE) require_once(DOC_ROOT_APP.'models/home.php');

// View
//======
if(MODE_TEMPLATE) require_once(DOC_ROOT_APP.'views/home.php');
$view               = 'home.inc.php';
$jsController       = 'home';

// Debugg
//=======
// $Gestion->debugg('off',$data);