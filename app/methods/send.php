<?php
require '../../vendor/phpmailer/class.phpmailer.php';

if(isset($_GET['sender-email']) && isset($_GET['recipient-name']) && isset($_GET['recipient-email']) && isset($_GET['model'])) {
    $mail = new PHPMailer;
    $mail->From = $_GET['sender-email'];
    $mail->FromName = $_GET['sender-email'];
    $mail->addAddress($_GET['recipient-email'], $_GET['recipient-name']);
    $mail->isHTML(true);
    $mail->Subject = 'Seat Compare';

    $body                   =   '';
    switch($_GET['model']) {
        case 'leon-st':
            $body    = 'This is the HTML message body <b>for Leon ST</b>';
        break;
        case 'leon-sc':
            $body    = 'This is the HTML message body <b>for Leon SC</b>';
        break;
        case 'leon':
            $body    = 'This is the HTML message body <b>for Leon</b>';
        break;
        case 'ibiza-sc':
            $body    = 'This is the HTML message body <b>for Ibiza SC</b>';
        break;
        case 'altea-xl':
            $body    = 'This is the HTML message body <b>for Altea XL</b>';
        break;
        case 'focus-sw':
            $body    = 'This is the HTML message body <b>Focus SW</b>';
        break;
    }
    $body               .=  'et l\'url a partag&eacute; est '.$_GET['url'];
    $mail->Body         =   $body;
    if(!$mail->send()) {
        echo json_encode(
            array(
            'success'=>0,
            'error'=>$mail->ErrorInfo
            )
        );
       exit;
    }
    echo json_encode(
        array(
        'success'=>1
        )
    );
}