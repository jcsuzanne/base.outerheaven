<?php
/**
 * OCS v2.0
 *
 * Classe Gestion
 * Bibliothèque de fonctions de traitement des données
 *
 * @version 0.1
 * @copyright 2011
 * @author jcsuzanne <jcsuzanne@ultranoir.com>
 * @package ocs
 */


class Gestion
{
	#############################################################################################################
	##################################################### DEFINITION VARIABLES ##################################
	#############################################################################################################
	/**
         *
         * @var string variables de d&eacute;finition
         */
	var $langue                = 'fr';
	var $email_admin           = 'jcsuzanne@ultranoir.com';
    var $mode_admin            = 'on';

	##############################################################################################################
	################################################### DEFINITION FONCTIONS #####################################
	##############################################################################################################


        /**
         * initialisation de la classe
         */
	function Gestion() {
		error_reporting(E_ALL);
	}

	function getJSONDatas($url,$params='') {
        $strParams = '';
        if(isset($params) && !empty($params)) {
            $strParams = '?'.http_build_query($params);
        }
        $url .= $strParams;
        $datas  = json_decode(file_get_contents($url));
        return $datas;
    }

	##################################################
	############## FONCTIONS STRING ##################
	##################################################


        /**
         *
         * traitement d'une chaine de caract&egrave;res. Supprime les accents de la chaine
         * @param string $texte chaine &agrave; traiter
         * @return string chaine trait&eacute;e
         */
	function NoAccent($texte){
		$letterE = array("&Egrave;","&Eacute;","&Ecirc;","&Euml;","&eacute;","&egrave;","&ecirc;","&euml;");
		$letterA = array("&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;","&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;");
		$letterO = array("&Ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&Oslash;","&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&oslash;");
		$letterC = array("&Ccedil;","&ccedil;");
		$letterI = array("&Igrave;","&Iacute;","&Icirc;","&Iuml;","&igrave;","&iacute;","&icirc;","&iuml;");
		$letterU = array("&Ugrave;","&Uacute;","&Ucirc;","&Uuml;","&ugrave;","&uacute;","&ucirc;","&uuml;");
		$letterY = array("&yuml;");
		$letterN = array("&Ntilde;","&ntilde;");
		$texte = str_replace($letterE,"e",$texte);
		$texte = str_replace($letterA,"a",$texte);
		$texte = str_replace($letterO,"o",$texte);
		$texte = str_replace($letterC,"c",$texte);
		$texte = str_replace($letterI,"i",$texte);
		$texte = str_replace($letterU,"u",$texte);
		$texte = str_replace($letterY,"y",$texte);
		$texte = str_replace($letterN,"n",$texte);
		return $texte;
   }


       /**
        *
        * traitement d'une chaine de caract&egrave;res. Supprime la ponctuation de la chaine
        * @param string $texte chaine &agrave; traiter
        * @return string chaine trait&eacute;e
        */
	function NoPunc($texte){
		$tb_ponctuation = array("-",",","!",":",";","+","/","?");
		$texte = str_replace($tb_ponctuation,"",$texte);
		$texte = str_replace(' ',"_",$texte);
		return $texte;
	}

        /**
         *
         * permet le troncage de texte
         * @param string $str chaine &agrave; tronquer
         * @param integer $limit le nombre de caract&egrave;res &agrave; prendre en compte dans la chaine avant troncage
         * @param string $glue Chaine de caract&egrave;res &agrave; ajouter si la chaine est tronqu&eacute;e
         * @return string Chaine tronqu&eacute;e
         */
	function str_cut($str, $limit=255,$glue='[...]') {
		if (strlen($str) < $limit) {
			return $str;
		}
		$str = strrev(substr($str, 0, $limit));
		return strrev(substr($str, strpos($str, ' '))) . $glue;
	}


        /**
         *
         * permet le troncage de texte
         * @param string $str chaine &agrave; tronquer
         * @param integer $limit le nombre de caract&egrave;res &agrave; prendre en compte dans la chaine avant troncage
         * @return string Chaine tronqu&eacute;e
         */
	function str_cut_check($str, $limit=255) {
		if (strlen($str) < $limit) {
			return false;
		}
		$str = strrev(substr($str, 0, $limit));
		return true;
	}


        /**
         *
         * ajoute un zero &agrave; une chaine
         * @param integer $number chaine &agrave; traiter
         * @return integer chaine trait&eacute;e
         */
	function addZero($number) {
		if(strlen($number) == 1) {
			return '0'.$number;
		} else {
			return $number;
		}
	}

	##################################################
	############## FONCTIONS DEBUGG ##################
	##################################################


        /**
         *
         * Sert pour le debugg. Normalisation de l'affichage du debugg
         * @param string $etat Activation/desactivation du debugg
         * @param array $string donn&eacute;es &agrave; afficher
         * @param string $color Couleur pour du texte de debugg
         */
	function debugg($etat,$string,$color=''){
        if ($etat == 'on' AND $this->mode_admin == 'on') {
            if ($color == '') {
                $color = "black";
            }
            print "<pre style='float:left;background-color:white;border:1px solid red;padding:5px;margin:5px;margin-bottom:25px;font-size:9px; font-family: Verdana; font-weight: bold;color:$color;'>";
            print_r($string);
            print "</pre>";
        }
     }


	############################################################
	############## FONCTIONS BROWSER + LANGUE ##################
	############################################################


        /**
         * detecte le navigateur utilis&eacute; pour la navigation. Cot&eacute; serveur
         * @return string Le navigateur utilis&eacute;
         */
	function return_browser()
	{
		 if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Mozilla"))
			$this->browser="Mozilla";

		 /* Netscape */
		 if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Netscape"))
		 	$this->browser = "Netscape";

		 /* Safari (Mac OS) */
		 else if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Safari"))
		 	$this->browser = "Safari";

		 /* FireFox */
		 else if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Firefox"))
			$this->browser = "Firefox";

		 /* Konqueror (Gnu/Linux KDE) */
		 else if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Konqueror"))
		 	$this->browser ="Konqueror";

		 /* Epiphany (Gnu/Linux Gnome) */
		 else if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Epiphany"))
		 	$this->browser ="Epiphany";

		 /* Lynx (text browser) */
		 else if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Lynx"))
		 	$this->browser ="Lynx";

		 /* Internet Explorer (Win32) */
		 /* Important: Internet Explorer test must be before Opera Test because
		 string "MSIE" is also present in Opera */
		 else if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "MSIE"))
			$this->browser ="Internet Explorer";

		 /* Opera */
		 if(strchr($_SERVER[ 'HTTP_USER_AGENT' ], "Opera"))
		 	$this->browser ="Opera";

		 return $this->browser;

	}

        /**
         *
         * detecte la langue utilis&eacute;e par l'utilisateur. Cot&eacute; serveur
         * @return string La langue utilis&eacute;e
         */
	function selectionLangue() {
		$langueNav = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
		$this->langue = $langueNav;
		return $this->langue;
	}


	####################################################
	############## FONCTIONS FICHIERS ##################
	####################################################

        /**
         *
         * retourne la taille d'un fichier
         * @param integer $fichier Valeur &agrave; traiter
         * @param boolean $nofloat Si renseign&eacute;e, valeur decimal envoy&eacute;e
         * @return string Taille du fichier en o,ko,mo,go
         */
	function taille_fichier($fichier,$nofloat='') {
		$taille_fichier = $fichier;
		if ($taille_fichier >= 1073741824) 	{
			if($nofloat > 0) {
				$taille_fichier = round(round($taille_fichier / 1073741824 * 100,0) / 100,$nofloat) . " Go";
			} else {
				$taille_fichier = round($taille_fichier / 1073741824 * 100) / 100 . " Go" ;
			}
		}
		elseif ($taille_fichier >= 1048576) {
			if($nofloat > 0) {
				$taille_fichier = round(round($taille_fichier / 1048576 * 100) / 100,$nofloat) . " Mo";
			} else {
				$taille_fichier = round($taille_fichier / 1048576 * 100) / 100 . " Mo";
			}
		}
		elseif ($taille_fichier >= 1024) {
			if($nofloat > 0) {
				$taille_fichier = round(round($taille_fichier / 1024 * 100) / 100,$nofloat) . " Ko";
			} else {
				$taille_fichier = round($taille_fichier / 1024 * 100) / 100 . " Ko";
			}
		} else 	{
			$taille_fichier = $taille_fichier . " o";
		}
		return $taille_fichier;
	}


	################################################
	############## FONCTIONS DATE ##################
	################################################



        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une date compl&egrave;te.
         * @param string $date Chaine date &agrave; traiter
         * @param string $langue Langue &agrave; utiliser pour le traitement de la chaine
         * @param boolean $only_jour Indique si le retour de la fonction ne renvoie que le jour de la date &agrave; traiter
         * @param string $glue Separateur &agrave; utiliser lors du traitement de la chaine
         * @param string $number Determine si le mois est affich&eacute; en chiffres ou en lettres
         * @param string $withday Determine si on ajoute le jour de la semaine dans la chaine &agrave; traiter
         * @param string $withyear Determine si on ajoute l'ann&eacute;e dans la chaine &agrave; traiter
         * @return string la date au format voulu
         */
	function genDate($date,$langue,$only_jour=0,$glue='',$number='',$withday='',$withyear='') {
		if(!isset($glue)) {
			$glue = " ";
		}
		if($langue == 'fr') {
			$tb_mois = array("00"=>"empty","01"=>"Janvier","02"=>"F&eacute;vrier","03"=>"Mars","04"=>"Avril","05"=>"Mai","06"=>"Juin","07"=>"Juillet","08"=>"Ao&ucirc;t","09"=>"Septembre","10"=>"Octobre","11"=>"Novembre","12"=>"D&eacute;cembre");
		} else if($langue == 'en') {
			$tb_mois = array("00"=>"empty","01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");
		} else {
			$tb_mois = array("00"=>"empty","01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");
		}
		if(substr($date,0,4) != '0000') {
			if($only_jour == 0){
				if($number) {
					$tr_date  = substr($date,6,2).$glue.substr($date,4,2).$glue.substr($date,0,4);
				} else if($withyear) {
					$tr_date  = 1*substr($date,6,2).$glue.$tb_mois[substr($date,4,2)];
				} else {
					$tr_date  = 1*substr($date,6,2).$glue.$tb_mois[substr($date,4,2)].$glue.substr($date,0,4);
				}
			}else{
				$tr_date  = substr($date,6,2);
			}
		} else {
			$tr_date = '';
		}
		if($withday!='') {
			$jour_fr = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
			$year = substr($date,0,4);
			$month = substr($date,4,2);
			$day = substr($date,6,2);
			$hour = substr($date,8,2);
			$min = substr($date,10,2);
			$sec = substr($date,12,2);

			$timestamp = mktime($hour,$min,$sec,$month,$day,$year);
			$indexDay = date("w", $timestamp);
			$nameDay = $jour_fr[$indexDay];
			$tr_date = $nameDay.' '.$tr_date;
			//return $nameDay;
		}
		return $tr_date;
	}

        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une date raccourcie.
         * @param string $date Chaine date &agrave; traiter
         * @param string $langue Langue &agrave; utiliser pour le traitement de la chaine
         * @return string la date au format voulu
         */
        function getMonthDate($date,$langue) {
            if($langue == 'fr') {
			$tb_mois = array("00"=>"empty","01"=>"Jan.","02"=>"F&eacute;v.","03"=>"Mars","04"=>"Avr.","05"=>"Mai","06"=>"Juin","07"=>"Juil.",
				 			 "08"=>"Aout","09"=>"Sept.","10"=>"Oct.","11"=>"Nov.","12"=>"D&eacute;c.");
		} else if($langue == 'en') {
			$tb_mois = array("00"=>"empty","01"=>"Jan.","02"=>"Fev.","03"=>"Mars","04"=>"Avr.","05"=>"Mai","06"=>"Juin","07"=>"Juil.",
				 			 "08"=>"Aout","09"=>"Sept.","10"=>"Oct.","11"=>"Nov.","12"=>"Dec.");
		} else if($langue == 'number') {
			$tb_mois = array("00"=>"empty","01"=>"01","02"=>"02","03"=>"03","04"=>"04","05"=>"05","06"=>"06","07"=>"07",
				 			 "08"=>"08","09"=>"09","10"=>"10","11"=>"11","12"=>"12");
		} else {
			$tb_mois = array("00"=>"empty","01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");
		}
             return $tb_mois[$date];
        }

        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une date raccourcie.
         * @param string $date Chaine date &agrave; traiter
         * @param string $langue Langue &agrave; utiliser pour le traitement de la chaine
         * @return string la date au format voulu
         */
	function genDate_mini($date,$langue) {
		if($langue == 'fr') {
			$tb_mois = array("00"=>"empty","01"=>"Jan.","02"=>"F&eacute;v.","03"=>"Mars","04"=>"Avr.","05"=>"Mai","06"=>"Juin","07"=>"Juil.",
				 			 "08"=>"Aout","09"=>"Sept.","10"=>"Oct.","11"=>"Nov.","12"=>"D&eacute;c.");
		} else if($langue == 'en') {
			$tb_mois = array("00"=>"empty","01"=>"Jan.","02"=>"Fev.","03"=>"Mars","04"=>"Avr.","05"=>"Mai","06"=>"Juin","07"=>"Juil.",
				 			 "08"=>"Aout","09"=>"Sept.","10"=>"Oct.","11"=>"Nov.","12"=>"Dec.");
		} else {
			$tb_mois = array("00"=>"empty","01"=>"January","02"=>"February","03"=>"March","04"=>"April","05"=>"May","06"=>"June","07"=>"July","08"=>"August","09"=>"September","10"=>"October","11"=>"November","12"=>"December");
		}

		$jour = substr($date,6,2);
		$mois = $tb_mois[substr($date,4,2)];
		$annee = substr($date,0,4);
		if($annee == "0000") {
			$tr_date = '';
		} else if($mois == "empty") {
			$tr_date = $annee;
		} else if($jour == '00') {
			$tr_date = $mois." ".$annee;
		} else {
			$tr_date  = $jour." ".$mois." ".$annee;
		}

		return $tr_date;
	}

        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une date explicite
         * @param string $date Chaine date &agrave; traiter
         * @return string Chaine trait&eacute;e
         */
	function formateDate($date) {
		$tr_date  = substr($date,6,2)."/".substr($date,4,2)."/".substr($date,0,4);
		return $tr_date;
	}


        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une heure explicite
         * @param string $time Chaine date &agrave; traiter
         * @return string Chaine trait&eacute;e
         */
	function formateTime($time) {
		$heure = substr($time,8,2);
		$heure *= 1;
		$tr_time  = $heure.":".substr($time,10,2);
        if($tr_time == '0:00') $tr_time = '';
		return $tr_time;
	}

         /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une heure explicite
         * @param string $time Chaine date &agrave; traiter
         * @return string Chaine trait&eacute;e
         */
	function formateTime_v2($time) {
		$tb_time = array("24"=>"00","25"=>"01","26"=>"02","27"=>"03","28"=>"04","29"=>"05","30"=>"06");
		$heure = substr($time,8,2);
		$heure *= 1;
		if($heure>=24) $heure = $tb_time[$heure];
		$tr_time  = $heure.":".substr($time,10,2);
		return $tr_time;
	}

        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une heure au dela des 24H
         * @param string $time Chaine date &agrave; traiter
         * @return string Chaine trait&eacute;e
         */
	function reverseTime($time) {
		$tb_time = array("00"=>"24","01"=>"25","02"=>"26","03"=>"27","04"=>"28","05"=>"29","06"=>"60");
		$heure = substr($time,8,2);
		if($heure>=00 && $heure<=06) $heure = $tb_time[$heure];
		$tr_time  = $heure.":".substr($time,10,2);
		return $tr_time;
	}

        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une date et une heure explicite
         * @param string $date Chaine date &agrave; traiter
         * @param string $second Si renseign&eacute;e, l'heure est ajout&eacute;e &agrave; la valeur de retour
         * @return string Chaine trait&eacute;e
         */
	function dateTime($date,$second='') {
		$tr_date  = substr($date,6,2)."/".substr($date,4,2)."/".substr($date,0,4).' &agrave; '.substr($date,8,2)."h".substr($date,10,2);
		if($second == true) {
			$tr_date  .= "m".substr($date,12,4).'s';
		}

		return $tr_date;
	}

        /**
         *
         * traitement d'une chaine de caract&egrave;res. G&eacute;n&egrave;re une dur&eacute;e explicite
         * @param string $data Chaine dur&eacute;e &agrave; traiter
         * @return string Chaine trait&eacute;e
         */
	function formateDuration($data){
		$duration_temp = $data;
		$hours  = substr($duration_temp,0,2);
		if($hours == '00'){
			$hours = '';
		}else{
			$hours *= 1;
			$hours = $hours.'h';

		}
		$minutes  = substr($duration_temp,3,2);
		if($minutes == '00'){
			$minutes = '';
		}else{
			$secondes  = substr($duration_temp,6,2);
			if($secondes > 30 ){
				$minutes = $minutes + 1;
			}
			$minutes *= 1;
			if(strlen($minutes)==1) $minutes = '0'.$minutes;
			if($hours == '00' || $hours == '') $minutes = $minutes.' min'; else $minutes = $minutes.'';
		}
		$duration = $hours.$minutes ;
		return $duration;
	}

    function convertDuration($data) {
        $convert = '';
        $Gestion = new Extend();
        if(isset($data) && !empty($data)) {
            $hour       = substr($data,0,2)*60;
            $min        = substr($data,3,2);
            $convert   = $hour+$min;
        }
        return $convert;
    }

    function get_elapsedtime($time=0) {
        /*
        @param $time: Unix timestamp
        Twitter passes you time like this: Fri Dec 18 23:28:16 +0000 2009
        You can convert that to a timestamp using strtotime()
        */


        $passed = time() - $time;

        //less than a minute
        if ($passed < 60) {
            return 'moins d\'une minute';
        }

        //less than an hour
        $minutes = round($passed / 60);
        if ($minutes < 60)  return 'il y a '.$minutes.' minute'.($minutes > 1 ? 's' : '').'';

        //less than 3 hours
        $hours = round($passed / 3600);
        if ($hours < 3) return 'il y a '.$hours.' heure'.($hours > 1 ? 's' : '').'';

        //today
        $hour = 3600;
        $oneday = 24*$hour;
        $midnight = strtotime("midnight");
        if ($time > $midnight) {
            $dawn = $midnight + 6*$hour;
            if ($time < $dawn) {
                $part_of_day = 'la nuit derni�re';
            } else if($time < ($midnight + 12*$hour)) {
                $part_of_day = 'ce matin';
            } else if($time < ($midnight + 18*$hour)) {
                $part_of_day = 'cet apr�s-midi';
            } else {
                $part_of_day = 'ce soir';
            }
            return $part_of_day.' '.date('H:i', $time);//Last night 04:12
        }

        //less than a week
        for ($days_ago=1; $days_ago < 7; $days_ago++) {
            $day = $midnight - $oneday*$days_ago;
            if ($time > $day) {
                if ($days_ago == 1) {
                    //yesterday
                    return 'hier '.date('H:i', $time);//Yesterday 15:12
                } else {
                    return date('l H:i', $time);//Tuesday 15:12
                }
            }
        }

        //more than a week
        return date('F jS H:i', $time);//like December 2nd 15:12
    }

    function getMedia($data,$format='preview_',$channel) {
        $url = '';
        if(isset($data) && !empty($data)) {
            $url = str_replace('[_format_]', $format, $data);
//            if(file_exists($filename))
        }
        return $url;
    }

      /**
     * Renvoi une chaine de caractere au format URL
     *
     * @param $str  (string) Chaine a convertir
     * @return      (string) Chaine au format URL
     */
    public static function MakeURL($str)
    {
        //  1ere verif : on supprime l'apostrophe de Word, on decode l'HTML et on met en minuscule
        $val = strtolower(html_entity_decode(str_replace('#039;',' ',$str)));

        //  1er niveau de verif par expression reguliere
        //  http://snipplr.com/view/26356/php--remove-accents/
        $match = array('/\./', '/â|ä|å|ã|á|à|Â|Ä|Å|Ã|Á|æ|Æ/','/ß/','/ç|Ç/','/Ð/','/é|è|ê|ë|É|È|Ê|Ë/','/ï|î|ì|í|Ï|Î|Ì|Í/','/ñ|Ñ/','/ö|ô|ó|ò|õ|Ó|Ô|Ö|Ò|Õ|œ|Œ/','/ù|û|ü|ú|Ü|Û|Ù|Ú/','/¥|Ý|ý|ÿ/','/ |\/|\\|\|/');
        $replace = array('', 'a', 'b', 'c', 'd', 'e', 'i', 'n', 'o', 'u', 'y', '-');
        $val = preg_replace($match, $replace, $val);

        //  2eme niveau de verif par autorisation
        $str_autorize = '*_abcdefghijklmnopqrstuvwxyz1234567890-';
        $return = '';
        for($i=0; $i<strlen($val); $i++) {
            if(strpos($str_autorize,$val[$i]) !== false) $return .= $val[$i];
        }
        //  Reduction des multiples underscore et suppression du dernier
        return preg_replace(array('/[-]{2,}/', '/-$/'), array('-', ''), $return);
    }

        /**
         *
         * execut&eacute; lors de la desctruction de l'instance
         */
	 function __destruct() {
	}


}
