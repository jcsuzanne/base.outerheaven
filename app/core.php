<?php
//  START BENCH
//=============
if(isset($_GET['timing'])) $start = microtime(true);

// Langue
//=======
$lang = 'fr';
if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    if(preg_match('/^(en)/', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $match)) {
        $lang = $match[1];
    }
}
if(isset($_GET['lang']) && !empty($_GET['lang']))
    $lang = $_GET['lang'];
else {
    header('HTTP/1.0 301 Moved Permanently');
    header('location:'.HTTP.$lang.'/');
    exit;
}

define('HTTP_LANG'          ,HTTP.$lang.'/');
define('WEB_ROOT_LANG'          ,WEB_ROOT.$lang.'/');
require_once(DOC_ROOT.'lang/lang.'.$lang.'.php');

// Mode
//=====
$mode                       = MODE_TEMPLATE ? 'dynamic/' : 'static/';
$minifySource               = MODE_COMPRESSION ? '.min' : '';

// Publication manager
//====================
$page = 'home';
// var_dump($_GET);
if(isset($_GET['page']) && !empty($_GET['page']))
    $page = $_GET['page'];

require_once(DOC_ROOT_APP.'controllers/'.$page.'.php');

//  404
//=====
if(isset($err404) && $err404) {
    header('HTTP/1.0 404 Not Found');
    exit(file_get_contents(ERR_404));
}

//  SHOW BENCH & CONSOLE
//======================
if(isset($_GET['timing'])) { echo (microtime(true)-$start).' sec &raquo; PUBLICATION DU FICHIER "'.DOC_ROOT_PUBLICATION.$pub.'"<br/>'; exit; }
if(isset($_GET['console'])) include_once('console.php');