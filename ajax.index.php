<?php
/**
 * Dispatch request & centralized actions
 */
header('Content-Type: text/html; charset=utf-8');
require_once 'conf.inc.php';
require_once('vendor/Mobile_Detect.php');
require_once('app/helpers/class.gestion.php');
require_once('app/helpers/functions.php');

$Gestion = new Extend;

// Device
//============
$device  = new Mobile_Detect;

// collect variable for the template
$dataTpl      = array('conf_phpJs' => $conf_phpJs);

// PUBLICATION
//============
require_once(DOC_ROOT_APP.'xhr.core.php');

if(isset($htmlConsole) && !empty($htmlConsole)) echo $htmlConsole;
