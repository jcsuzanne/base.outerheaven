<?php
/**
 * Dispatch request & centralized actions
 */
header('Content-Type: text/html; charset=utf-8');
require_once 'conf.inc.php';
require_once('vendor/Mobile_Detect.php');
require_once('app/helpers/class.gestion.php');
require_once('app/helpers/functions.php');

$Gestion = new Extend;

// Device
//============
$device  = new Mobile_Detect;
// $deviceType = ($device->isMobile() ? ($device->isTablet() ? 'tablet' : 'phone') : 'computer');

// PUBLICATION
//============
require_once(DOC_ROOT_APP.'core.php');

// DISPLAY
//=======+
$viewFile = DOC_ROOT.$mode.$view;

if(!MODE_TEMPLATE) {
    $dataView = explode('.',$view);
    $jsController = $dataView[0];
}

if(isset($_GET['tpl'])) {
     ob_start();
     if(file_exists($viewFile)) require_once($viewFile);
    echo json_encode(
        array(
              'view'=>ob_get_clean(),
              'meta'=>
                array(
                    'title'=>$metaTitle,
                ),
               'js'=>
                array(
                    'controller'=>$jsController,
                    "method"=> ""
                ),
              'url'=>'/'.str_replace(HTTP, '', $metaUrl)
            )
        );
}
else {
    if(isset($_GET['xhr']))
    {
        if(file_exists($viewFile)) require_once($viewFile);
    }
    else
    {
        require_once(DOC_ROOT_INCLUDE.'com.builder.inc.php');
        require_once(DOC_ROOT_INCLUDE.'com.header.inc.php');
        if(file_exists($viewFile)) require_once($viewFile);
        require_once(DOC_ROOT_INCLUDE.'com.footer.inc.php');
        require_once(DOC_ROOT_INCLUDE.'com.manager.inc.php');
    }

}

if(isset($htmlConsole) && !empty($htmlConsole)) echo $htmlConsole;
