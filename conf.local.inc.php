<?php
/**
 * conf locale
 * override defined constant in config.inc.php
 **/
$conf_phpJs['WEB_ROOT']             = '/base.outerheaven/';
$conf_phpJs['WEB_ROOT_COMPONENT']   = $conf_phpJs['WEB_ROOT'].'assets/';
$conf_phpJs['WEB_ROOT_IMG']         = $conf_phpJs['WEB_ROOT_COMPONENT'].'img/';
$conf_phpJs['WEB_ROOT_CSS']         = $conf_phpJs['WEB_ROOT_COMPONENT'].'css/';
$conf_phpJs['WEB_ROOT_LESS']        = $conf_phpJs['WEB_ROOT_COMPONENT'].'less/';
$conf_phpJs['WEB_ROOT_JS']          = $conf_phpJs['WEB_ROOT_COMPONENT'].'js/';
$conf_phpJs['WEB_ROOT_LIB']         = $conf_phpJs['WEB_ROOT_COMPONENT'].'lib/';
$conf_phpJs['WEB_ROOT_BUILD']       = $conf_phpJs['WEB_ROOT_COMPONENT'].'build/';
$conf_phpJs['WEB_ROOT_VENDOR']      = $conf_phpJs['WEB_ROOT'].'vendor/';

$conf_phpJs['HTTP']               = 'http://'.$_SERVER['HTTP_HOST'].$conf_phpJs['WEB_ROOT'];

$conf_phpJs['MODE_COMPRESSION']     = false;
$conf_phpJs['MODE_TEMPLATE']        = false;
$conf_phpOnly['SHOW_DEBUG']         = false;