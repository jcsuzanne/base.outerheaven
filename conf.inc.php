<?php
/**
 * Project configuration.
 */

//	DEV MODE
//	========
// Enable all error logging while in development
ini_set('display_errors', 'on');
error_reporting(E_ALL);

//ABSOLUTES & RELATIVES PATH
//	=======================
define('DOC_ROOT'					, dirname(__FILE__).'/');
define('DOC_ROOT_DATA'				, DOC_ROOT.'data/');
define('DOC_ROOT_INCLUDE'			, DOC_ROOT.'include/');
define('DOC_ROOT_LANGUE'			, DOC_ROOT.'lang/');
define('DOC_ROOT_APP'               , DOC_ROOT.'app/');
define('ERR_404'					, DOC_ROOT.'404.html');

//CLIENT & SERVER SIDE CONFIGURATION
//==================================
$conf_phpJs['WEB_ROOT']				= '/';
$conf_phpJs['WEB_ROOT_DATA']        = $conf_phpJs['WEB_ROOT'].'data/';
$conf_phpJs['WEB_ROOT_COMPONENT']   = $conf_phpJs['WEB_ROOT'].'assets/';
$conf_phpJs['WEB_ROOT_IMG']         = $conf_phpJs['WEB_ROOT_COMPONENT'].'img/';
$conf_phpJs['WEB_ROOT_CSS']         = $conf_phpJs['WEB_ROOT_COMPONENT'].'css/';
$conf_phpJs['WEB_ROOT_LESS']        = $conf_phpJs['WEB_ROOT_COMPONENT'].'less/';
$conf_phpJs['WEB_ROOT_JS']          = $conf_phpJs['WEB_ROOT_COMPONENT'].'js/';
$conf_phpJs['WEB_ROOT_LIB']         = $conf_phpJs['WEB_ROOT_COMPONENT'].'lib/';
$conf_phpJs['WEB_ROOT_BUILD']       = $conf_phpJs['WEB_ROOT_COMPONENT'].'build/';
$conf_phpJs['WEB_ROOT_VENDOR']      = $conf_phpJs['WEB_ROOT'].'vendor/';
$conf_phpJs['WEB_ROOT_TMP']         = $conf_phpJs['WEB_ROOT'].'tmp/';
$conf_phpJs['IP_CLIENT']                = $_SERVER['REMOTE_ADDR'];

if(isset($_SERVER['HTTP_HOST'])) {
	$conf_phpJs['HTTP']			    = 'http://'.$_SERVER['HTTP_HOST'].$conf_phpJs['WEB_ROOT'];
	$conf_phpJs['HTTP_DATA']		= 'http://'.$_SERVER['HTTP_HOST'].$conf_phpJs['WEB_ROOT_DATA'];
	define('URI'					, 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    define('REQUEST', trim(str_replace($conf_phpJs['WEB_ROOT'], '', $_SERVER['REQUEST_URI']), '/'));
}

//  ========================================================================================================================
//  FRONTOFFICE CONFIGURATION
//  ========================================================================================================================
$conf_phpJs['MODE_TEMPLATE']        = true;
$conf_phpJs['MODE_COMPRESSION']     = true;
$conf_phpOnly['SHOW_DEBUG']         = false;

if(is_file(dirname(__FILE__).'/conf.local.inc.php')){
    require_once (dirname(__FILE__).'/conf.local.inc.php');
}

//  ========================================================================================================================
//  DEFINE SERVER SIDE CONFIGURATION
//  ========================================================================================================================
foreach($conf_phpJs as $k=>$v) {
    eval('define(\''.$k.'\', \''.$v.'\');');
}
foreach($conf_phpOnly as $k=>$v) {
    eval('define(\''.$k.'\', \''.$v.'\');');
}
?>