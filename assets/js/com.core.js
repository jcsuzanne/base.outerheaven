kickstarter.bootstrap('SITE', {
    ajax: {
        durationFadeIn: 800,
        durationFadeOut: 800
    }
});

//variables
//=========
var
    $mainContent
,   $pageTop
,   $body
,   $master
,   $mainNav
,   $window             = $(window)
,   _k                  = kickstarter
,   isVisiting          = false
,   _DEBUGG             = false
,   isOldie             = false
,   no3D                = false
,   isTouch             = false
,   isTablet            = false
,   startApp            = false
,   API_transition
,   allowPreventTouch   = true
;

(function(window, document, undefined) {
    'use strict';

    SITE.core = (function()
    {

        var detectBrowser = function() {
            if(!Modernizr.csstransforms) {
                no3D = true;
            }
            if(kickstarter.ev.move == 'touchmove') {
                isTouch = true;
            }
            Detectizr.detect({
                addAllFeaturesAsClass: false
            ,   detectDevice: true
            ,   detectDeviceModel: false
            ,   detectScreen: false
            ,   detectOS: true
            ,   detectBrowser: true
            ,   detectPlugins: false
            })
            if(Modernizr.Detectizr.device.type == 'tablet') {
                isTablet = true;
                preventTouchBounce();
            }
        }

        //master finalize
        //================
        var finalize = function()
        {

        }

        //master init
        //===========
        var init = function()
        {
            var scope = this;
            API_transition      =   transition.init();
            setGlobalVariables();
            smartResize();
            detectBrowser();
            scope.ready();
        }

        // Datas Is Ready
        //==============
        var ready = function() {
            SITE.toolbox.init();
            SITE.layout.init();
        }

        // Do a clean resize bind
        //=======================
        var smartResize = function() {
            $(window).on("throttledresize", function( event ) {
                kickstarter.publish('window::smartresize');
            });
        }

        //set global element in cache
        //===========================
        var setGlobalVariables = function() {
            $mainContent        = $('#main-content');
            $pageTop            = $('html, body');
            $body               = $('body');
            $master             = $('#master');
            $mainNav            = $('#main-nav');
        }

        return {
            'ready': ready,
            'init': init,
            'finalize': finalize,
            'smartResize':smartResize

        };
    })();

}(window, document));