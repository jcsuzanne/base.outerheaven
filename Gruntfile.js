module.exports = function(grunt) {
    // Project configuration.
    //=======================
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.name %>  \n' +
                ' *\n' +
                ' * @version   v<%= pkg.version %>\n' +
                ' * @build     <%= grunt.template.today("dd/mm/yyyy HH:MM") %>\n' +
                ' */\n',
        jshint: {
            all: ['Gruntfile.js', 'assets/js/*.js']
        }
        ,
        sprite: {
            dist: {
                src:        ['assets/img/sprite/*.png'],
                destImg:    'assets/img/common/sprite.png',
                destCSS:    'assets/precss/sprite.scss',
                cssFormat:  'scss',
                imgPath:    '../img/common/sprite.png',
                algorithm:  'left-right'
            }
        }
        ,
        sass: {
            dist: {
                files: {
                    'assets/build/front.css': 'assets/precss/builder.scss'
                }
            }
        }
        ,
        concat: {
            options: {
                separator: ';'
            },
            css: {
                src: [
                       'assets/css/com.reset.css',
                       'assets/css/com.utils.css',
                       'assets/css/lib.gcolumns.css',
                       'assets/css/com.animation.css',
                       'assets/lib/css3/animate.css'
                ],
                dest:  'assets/build/top.css'
            },
            jstop: {
                src: [
                       'assets/lib/modernizr/modernizr.custom.js',
                       'assets/lib/modernizr/detectizr.min.js'
                ],
                dest:  'assets/build/top.js'
            },
            js: {
                src: [
                       'assets/lib/json/json2.js',
                       'assets/lib/jquery/jquery-2.1.0.min.js',
                       'assets/lib/jquery-upgrade/jquery.enhance-1.2.js',
                       'assets/lib/jquery-upgrade/jquery.mousewheel.js',
                       'assets/lib/jquery-upgrade/jquery.outside.js',
                       // 'assets/lib/history/jquery.history.js',
                       'assets/lib/kickstarter/kickstarter.js',
                       'assets/lib/gsap/TweenMax.min.js',
                       'assets/js/com.transition.js',
                       'assets/js/com.core.js',
                       'assets/js/com.toolbox.js',
                       'assets/js/mod.layout.js',
                       'assets/js/mod.navigation.js',
                       'assets/js/app.js'
                ],
                dest: 'assets/build/front.js'
            }
        }
        ,
        cssmin: {
            compress: {
                files: {
                    'assets/build/top.min.css': ['assets/build/top.css'],
                    'assets/build/front.min.css': ['assets/build/front.css']
                }
            }
        }
        ,
        uglify: {
            build: {
                files: {
                    'assets/build/top.min.js': ['assets/build/top.js'],
                    'assets/build/front.min.js': ['assets/build/front.js']
                }
            }
        }
        ,
        imageoptim: {
          assets: {
            options: {
              jpegMini: false,
              imageAlpha: true,
              quitAfter: true
            },
            src: ['assets/img']
          }
        }
        ,
        watch: {
            options: {
              livereload: true,
            },
            assets: {
                files: ['assets/precss/*.scss', 'assets/js/*.js','assets/css/*.css'],
                tasks: [
                    'sass',
                    'concat',
                ]
            }
        }
        ,
        git_ftp: {
            dev: {
                options: {
                    'hostFile': '.gitftppass',
                    'host': 'dev'
                }
            }
        }
        ,
        throttle: {
            default: {
                remote_port: '80',
                local_port: 8001,
                upstream: 10*1024,
                downstream: 100*1024,
                keepalive: true
            }
        }
    });

    // Load the plugins
    //=================
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-git-ftp');
    grunt.loadNpmTasks('grunt-imageoptim');
    grunt.loadNpmTasks('grunt-throttle');

    // Default task(s).
    //=================
    grunt.registerTask('default',   ['sass','concat','cssmin','uglify']);
    grunt.registerTask('gensprite', ['sprite','sass','concat','cssmin','uglify']);
    grunt.registerTask('prod',      ['sass','concat','cssmin','uglify']);
    grunt.registerTask('ftpdev',    ['git_ftp:dev']);
    grunt.registerTask('imgopti',   ['imageoptim:assets']);
    grunt.registerTask('connect',   ['throttle']);

    // Task watcher
    //=============
    grunt.registerTask('assets', [
        'watch:assets'
    ]);

};