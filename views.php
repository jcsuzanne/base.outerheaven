<?php
require_once 'conf.inc.php';
function getViews()
{
    $path = DOC_ROOT.'views/';
    $templates = array();
    $temp = array();
    if ($handle = opendir($path)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {
                $name = str_replace('.php', '', $file);
                $html = file_get_contents($path.$file);
                $html = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $html);
                $html = json_encode($html);
                array_push($temp, '"'.$name.'":'.$html);
            }
        }
        closedir($handle);
    }
    if(count($temp)>0) {
        $templates = implode(',',$temp);
        $templates = 'BASE.templates = { '.$templates. '};';
    }
    header('Content-type: application/json');
    return $templates;
}
echo getViews();