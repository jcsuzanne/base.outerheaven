<?php
/**
 * Client side configuration.
 * @copyright Copyright (C) 2011 ultranoir
 */
require_once('conf.inc.php');
header('Content-Type: text/javascript');

//	========================================================================================================================
//	WRITE JAVASCRIPT CONFIGURATION
//	========================================================================================================================
?>
//<![CDATA[
<?php
foreach($conf_phpJs as $k=>$v) {
	echo "var $k = '$v';
";
}
?>