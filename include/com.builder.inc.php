<!doctype html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html class="no-js ie6 oldie" lang="<?= $lang ?>"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7 oldie" lang="<?= $lang ?>"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8 oldie" lang="<?= $lang ?>"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="<?= $lang ?>"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="<?= $lang ?>"> <!--<![endif]-->
<head>
    <title><?= _META_TITLE ?></title>
    <meta charset="utf-8">
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="<?= URI ?>" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo WEB_ROOT_IMG ?>favicon.ico">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT_BUILD ?>top<?php echo $minifySource; ?>.css" media="all" />
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT_BUILD ?>front<?php echo $minifySource; ?>.css" media="all" />
    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo WEB_ROOT_BUILD ?>top<?php echo $minifySource; ?>.js"></script>
    <script type="text/javascript">
    // <![CDATA[
    <?php
    foreach($conf_phpJs as $k=>$v) {
        echo "var $k='$v';";
    }
    ?>
    var config = {
        lang : '<?= $lang ?>',
        httpLang : '<?= HTTP_LANG ?>',
        webrootLang : '<?= WEB_ROOT_LANG ?>'
    }
    // ]]>
    </script>
</head>
<body role="document" data-controller="<?= $jsController ?>" data-method data-event>
    <div id="master">